import dash
import dash_bootstrap_components as dbc

app = dash.Dash(
    __name__,
    title="COVID-19 Latvia",
    suppress_callback_exceptions=True,
    external_stylesheets=[
        dbc.themes.BOOTSTRAP,
        {
            "rel": "icon",
            "type": "image/svg+xml",
            "href": "/assets/favicon.svg",
        },
    ],
    external_scripts=[
        {
            "src": "https://kit.fontawesome.com/d4bac8c420.js",
            "crossOrigin": "anonymous"
        },
    ],
)
