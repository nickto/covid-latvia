import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app import app
from dashboard import overview
from dashboard import daily
from dashboard import two_weeks
from dashboard import app_data
from dashboard import vaccination
from dashboard import hospitalizations
from dashboard import about

server = app.server

app.layout = html.Div(
    [dcc.Location(id="url", refresh=False),
     html.Div(id="page-content")])


@app.callback(Output("page-content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname in ("/", "/overview"):
        return overview.layout
    elif pathname in ("/daily"):
        return daily.layout
    elif pathname in ("/14day"):
        return two_weeks.layout
    elif pathname in ("/vaccination"):
        return vaccination.layout
    elif pathname in ("/hospitalizations"):
        return hospitalizations.layout
    elif pathname in ("/about"):
        return about.layout
    elif pathname in ("/_ah/warmup"):  # Google App Engine warm up request
        # Run cached functions
        app_data.get_configs()
        app_data.read_cases()
        app_data.read_cases_meta()
        app_data.read_vaccination_daily()
        return html.Div()
    else:
        return "404"


if __name__ == "__main__":
    app.run_server(debug=True, host="0.0.0.0", port="8051", threaded=True)