import logging
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State

from app import app

from dashboard import navbar

ID_PREFIX = "about"

# yapf: disable
layout = dbc.Container([
    navbar.gen_layout(active="About"),
    dbc.Container([
        html.H1("About"),
        html.Div([
            html.P("Data sources:"),
            html.Ul([
                html.Li(html.Span([
                    html.A("COVID-19 cases", href="https://data.gov.lv/dati/lv/dataset/covid-19"),
                    " from the Latvian Open data portal."
                ])),
                html.Li(html.Span([
                    html.A("COVID-19 vaccinations", href="https://data.gov.lv/dati/lv/dataset/covid19-vakcinacijas"),
                    " from the Latvian Open data portal."
                ])),
                html.Li(html.Span([
                    html.A("COVID-19 hospitalizations", href="https://data.gov.lv/dati/lv/dataset/stacionaru-operativie-dati-par-covid19"),
                    " from the Latvian Open data portal."
                ])),
                html.Li(html.Span([
                    html.A("Population data", href="https://data.gov.lv/dati/eng/dataset/latvijas-iedzivotaju-skaits-pasvaldibas"),
                    " from the Latvian Open data portal."
                ])),
            ]),
        ]),
        html.Div([
            html.P("Source code:"),
            html.Ul([
                html.Li(html.Span([
                    html.A("GitLab", href="https://gitlab.com/nickto/covid-latvia/"),
                    "."
                ])),
            ]),
        ]),
    ])
], id=ID_PREFIX + "-container")
# yapf: enable