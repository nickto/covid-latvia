import logging
import dash
from dash_bootstrap_components._components.CardBody import CardBody
from dash_bootstrap_components._components.CardHeader import CardHeader
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from dashboard import app_data
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd

from app import app

from dashboard import navbar
from dashboard import plots

ID_PREFIX = "vaccination"

# yapf: disable
layout = dbc.Container([
    navbar.gen_layout(active="Vaccination"),
    dbc.Container([
        html.H1("Vaccination"),
        dbc.Card([
            dbc.CardHeader("Cumulative vaccinations"),
            dbc.CardBody(id=ID_PREFIX + "-cum-vaccinations-daily-shots"),
        ], style={"margin-top": "1ex"}),
        dbc.Card([
            dbc.CardHeader("Vaccinations daily"),
            dbc.CardBody(id=ID_PREFIX + "-vaccinations-daily-shots"),
        ], style={"margin-top": "1ex"}),
        dbc.Card([
            dbc.CardHeader("Vaccinations by vaccine"),
            dbc.CardBody(id=ID_PREFIX + "-vaccinations-by-vaccine"),
        ], style={"margin-top": "1ex"}),
    ])
], id=ID_PREFIX + "-container")
# yapf: enable

plots.gen_callbacks_for_multiple_cols_1_axis_plot(
    df=app_data.read_vaccination_daily(),
    x="date",
    ys=["round_1", "round_2"],
    y_names=["1st dose", "2nd dose"],
    initial_trigger_id=ID_PREFIX + "-container",
    parent_id=ID_PREFIX + "-vaccinations-daily-shots",
    app=app,
    hovertemplate="<b>%{x}</b><br>%{y:,.0f}<extra></extra>",
    y_axis_title="Vaccine shots",
)

plots.gen_callbacks_for_multiple_cols_1_axis_plot(
    df=app_data.read_vaccination_daily(),
    x="date",
    ys=["round_1_cum", "round_2_cum"],
    y_names=["1 dose", "2 doses"],
    initial_trigger_id=ID_PREFIX + "-container",
    parent_id=ID_PREFIX + "-cum-vaccinations-daily-shots",
    app=app,
    hovertemplate="<b>%{x}</b><br>%{y:,.0f}<extra></extra>",
    y_axis_title="Vaccine shots",
)


@app.callback(
    Output(ID_PREFIX + "-vaccinations-by-vaccine", "children"),
    Input(ID_PREFIX + "-container", "children"),
)
def plot_vaccines_by_vaccine(_):
    dose_to_name = {
        1: "First dose",
        2: "Second dose",
    }
    does_to_color = {
        1: "rgb(99, 110, 250)",
        2: "rgb(239, 85, 59)",
    }

    df = app_data.read_vaccination_by_vaccine()
    df = df.merge(
        df.groupby("vaccine_name").agg(vaccine_total=("count",
                                                      "sum")).reset_index(),
        on="vaccine_name",
        how="left",
    )

    fig = go.Figure()
    for i in sorted(df.loc[:, "vaccination_round"].unique()):
        df_trace = df.query(f"vaccination_round == {i}")
        fig.add_trace(
            go.Bar(
                name=dose_to_name[i],
                y=df_trace.loc[:, "vaccine_name"],
                x=df_trace.loc[:, "count"],
                orientation="h",
                text=df_trace.loc[:, "vaccine_total"],
                marker_color=does_to_color[i],
                hovertemplate=("<b>Vaccine:</b> %{y}<br>" +
                               ("<b>First doses:</b>" if i == 1 else
                                "<b>Second doses: </b>") + "%{x:,.0f}<br>" +
                               "<b>Total: </b> %{text:,.0f}<br>"),
            ))

    fig.update_layout(
        barmode="stack",
        yaxis={
            "categoryorder": "total ascending",
            "title": None
        },
        xaxis={"title": "Number of doses"},
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1,
            xanchor="right",
            x=1,
            title=None,
        ),
    )
    fig.layout.margin = go.layout.Margin(t=0, b=0, l=0, r=0)

    return dcc.Graph(
        figure=fig,
        config={
            "displaylogo": False,
            "displayModeBar": False,
            "modeBarButtons": [],
        },
    )
