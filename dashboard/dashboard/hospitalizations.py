import logging
import dash
from dash_bootstrap_components._components.CardBody import CardBody
from dash_bootstrap_components._components.CardHeader import CardHeader
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from dashboard import app_data
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd

from app import app

from dashboard import navbar
from dashboard import plots

ID_PREFIX = "hospitalizations"

# yapf: disable
layout = dbc.Container([
    navbar.gen_layout(active="Hospitalizations"),
    dbc.Container([
        html.H1("Hospitalizations"),
        dbc.Card([
            dbc.CardHeader("Hospitalizations"),
            dbc.CardBody(id=ID_PREFIX + "-hospitalizations"),
        ], style={"margin-top": "1ex"}),
        # dbc.Card([
        #     dbc.CardHeader("Vaccinations daily"),
        #     dbc.CardBody(id=ID_PREFIX + "-vaccinations-daily-shots"),
        # ], style={"margin-top": "1ex"}),
    ])
], id=ID_PREFIX + "-container")
# yapf: enable

plots.gen_callbacks_for_multiple_cols_1_axis_plot(
    df=app_data.read_hospitalizations(),
    x="date",
    ys=["hospitalized", "hospitalized_severe", "hospitalized_moderate"],
    y_names=["Total", "Critical condition", "Moderate condition"],
    initial_trigger_id=ID_PREFIX + "-container",
    parent_id=ID_PREFIX + "-hospitalizations",
    app=app,
    hovertemplate="<b>%{x}</b><br>%{y:,.0f}<extra></extra>",
    y_axis_title="Hospitalizations",
)