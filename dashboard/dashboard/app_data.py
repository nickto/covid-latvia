import pandas as pd
import logging
import functools
import os
import yaml
from google.cloud import storage
from google.api_core.exceptions import Forbidden
from io import StringIO
import cachetools.func

CACHE_TTL = 600  # seconds


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def get_configs():
    return yaml.safe_load(open("configs.yaml", "r"))


def read_csv(key):
    configs = get_configs()

    try:
        bucket = configs["gcp"]["bucket"]
        filepath = configs["processed"][key]["path"]

        client = storage.Client()
        bucket = client.get_bucket(configs["gcp"]["bucket"])
        blob = bucket.get_blob(filepath)
        csv_as_string = blob.download_as_text()

        return pd.read_csv(StringIO(csv_as_string))
    except Forbidden as e:
        logging.warning("Could not read data froum cloud. Attempting to read locally.")

        project_home = configs["local"]["project_home"]
        filepath = configs["processed"][key]["path"]

        path = os.path.join(project_home, filepath)

        return pd.read_csv(path)


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def read_cases():
    df = read_csv("cases_daily")
    df = df.sort_values("date")
    return df


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def read_cases_meta():
    configs = get_configs()

    filepath = configs["meta"]["cases"]["path"]

    try:
        # Instantiates a client
        client = storage.Client()
        bucket = client.get_bucket(configs["gcp"]["bucket"])
        blob = bucket.get_blob(filepath)
        meta_as_string = blob.download_as_string()

        return yaml.safe_load(meta_as_string)
    except Forbidden as e:
        logging.warning("Could not read metadata froum cloud. Attempting to read locally.")
        project_home = configs["local"]["project_home"]
        path = os.path.join(project_home, filepath)

        return yaml.safe_load(open(path, "r"))


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def read_vaccination_daily():
    df = read_csv("vaccination_daily")
    df = df.sort_values("date")
    return df


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def read_vaccination_categories():
    df = read_csv("vaccination_categories")
    df = df.sort_values("date")
    return df


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def read_vaccination_by_vaccine():
    df = read_csv("vaccination_by_vaccine")
    return df


@cachetools.func.ttl_cache(maxsize=1, ttl=CACHE_TTL)
def read_hospitalizations():
    df = read_csv("hospitalizations")
    df = df.sort_values("date")
    return df
