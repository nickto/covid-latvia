# COVID-19

## Links:

- https://data.gov.lv/dati/lv/dataset/covid-19
- https://data.gov.lv/dati/lv/dataset/covid19-vakcinacijas


## Useful endpoints

- View all available packages (datasets): 
  `https://data.gov.lv/dati/eng/api/3/action/package_list`. 
- Get help for the endpoints (e.g., for the one above):
  `https://data.gov.lv/dati/eng/api/3/action/help_show?name=package_list`.
- Show details of the package (e.g., for `covid-19`):
  `https://data.gov.lv/dati/eng/api/3/action/package_show?id=covid-19`.

## Developer notes

### Local development

#### Local development set up

On Linux (Mac?) with `make` and [direnv](https://direnv.net/) present:

1. Create `.env` using `.env.template` as a template.
2. Create a virtual environment
```bash
make .venv
```
3. Create `.envrc`
```bash
make .envrc
direnv allow
```

If `make` and `direnv` not present.

1. Create virtal environemnt and activate it.
2. Install requirements from `update/requirements.txt` and `dashboard/requirements.txt`
3. Export env variables from `.env.template`. 

#### Update data

```
cd update
python main.py --skip-upload
```

#### Dashboard

```
cd dashboard
python index.py
```

### Terraform locally 

Follow
[these steps](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#get-started-using-local-development)
to make Terraform work locally without breaking deployment via CI pipelines.

## TODO

- [X] Make warmup requests.
- [X] Change title.
- [X] Change favicon.
- [X] Try submitting the same version in App Engine.
- [X] Add vaccination data.
- [ ] Add weekly overlays.
- [X] Make warmup actually cache data.
- [X] Make main protected branch and allow secrets only in protected branch.
- [ ] Single source of truth for domain name (hence, bucket names as well).
- [ ] Build zips in GitLab CI instead of hacky way in Terraform.
- [X] Brush up grids on the plots with multiple axis.


