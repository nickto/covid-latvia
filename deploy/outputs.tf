output "dns_records" {
  value = google_app_engine_domain_mapping.default.resource_records
}
