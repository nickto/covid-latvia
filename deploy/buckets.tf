# Bucket for data
resource "google_storage_bucket" "data_bucket" {
  name                        = "bucket.data.${var.domain}"
  location                    = "europe-west3"
  storage_class               = "STANDARD"
  force_destroy               = true
  uniform_bucket_level_access = false

  versioning {
    enabled = false
  }
}

# Bucket for source code
resource "google_storage_bucket" "source_bucket" {
  name                        = "bucket.source.${var.domain}"
  location                    = "europe-west3"
  storage_class               = "STANDARD"
  force_destroy               = true
  uniform_bucket_level_access = false

  versioning {
    enabled = false
  }
}
