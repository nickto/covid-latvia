terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }

  }
}

provider "google" {
  credentials = file(var.service_account_key)
  project     = var.project
  region      = var.region
  zone        = var.zone
}

# Service Accounts
resource "google_service_account" "scheduler_account" {
  account_id   = "scheduler-service-account"
  display_name = "Service Account of Scheduler"
}
