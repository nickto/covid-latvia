resource "google_app_engine_application" "app" {
  project     = var.project
  location_id = var.region
}

resource "google_app_engine_domain_mapping" "default" {
  domain_name = var.domain

  ssl_settings {
    ssl_management_type = "AUTOMATIC"
  }
}

data "archive_file" "dashboard" {
  type        = "zip"
  source_dir  = "../dashboard/"
  output_path = "./dashboard.zip"
}

resource "google_storage_bucket_object" "dashboard" {
  # Hashing in the end of file name is needed to trigger changes when source code changes
  # Ugly, but it seems like it's the only workaround so far:
  # https://github.com/hashicorp/terraform-provider-google/issues/1938
  name   = "dashboard.zip#${data.archive_file.dashboard.output_md5}"
  bucket = google_storage_bucket.source_bucket.name
  source = data.archive_file.dashboard.output_path
}

resource "google_app_engine_standard_app_version" "default" {
  service    = "default"
  version_id = "deployed"

  runtime        = "python38"
  instance_class = "F1"

  entrypoint {
    shell = "gunicorn -b :$PORT index:server"
  }

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.source_bucket.name}/${google_storage_bucket_object.dashboard.name}"
    }
  }

  automatic_scaling {
    min_idle_instances = 0
    max_idle_instances = 1
    standard_scheduler_settings {
      target_cpu_utilization = 0.95
      target_throughput_utilization = 0.75
      min_instances = 0
      max_instances = 5
    }      
  }

  noop_on_destroy = true
  delete_service_on_destroy = true

  inbound_services = ["INBOUND_SERVICE_WARMUP"]

  handlers {
    url_regex                   = ".*"
    security_level              = "SECURE_ALWAYS"
    redirect_http_response_code = "REDIRECT_HTTP_RESPONSE_CODE_301"
    script {
      script_path = "auto"
    }    
  }
}