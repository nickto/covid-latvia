variable "project" {}

variable "service_account_key" {
  type = string
}

variable "region" {
  default = "europe-west3"
}

variable "zone" {
  default = "europe-west3-a"
}

variable "domain" {}
