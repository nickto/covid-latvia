data "archive_file" "update" {
  type        = "zip"
  source_dir  = "../update/"
  output_path = "./update.zip"
}

resource "google_storage_bucket_object" "update" {
  # Hashing in the end of file name is needed to trigger changes when source code changes
  # Ugly, but it seems like it's the only workaround so far:
  # https://github.com/hashicorp/terraform-provider-google/issues/1938
  name   = "update.zip#${data.archive_file.update.output_md5}"
  bucket = google_storage_bucket.source_bucket.name
  source = data.archive_file.update.output_path
}

# Cloud function
resource "google_cloudfunctions_function" "update" {
  name        = "update"
  description = "Update COVID data"
  runtime     = "python38"

  available_memory_mb   = 1024 
  timeout               = 300
  source_archive_bucket = google_storage_bucket.source_bucket.name
  source_archive_object = google_storage_bucket_object.update.name
  trigger_http          = true
  ingress_settings      = "ALLOW_ALL"
  entry_point           = "get"

  environment_variables = {
    RAW_DATA_PATH       = "/tmp/data/raw/"
    CLEAN_DATA_PATH     = "/tmp/data/clean/"
    PROCESSED_DATA_PATH = "/tmp/data/processed/"
  }  
}

# IAM entry for a Scheduler Service Account to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.update.project
  region         = google_cloudfunctions_function.update.region
  cloud_function = google_cloudfunctions_function.update.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${google_service_account.scheduler_account.email}"
}

resource "google_cloud_scheduler_job" "update_trigger" {
  name             = "update-trigger"
  description      = "Trigger data update"
  schedule         = "0 */2 * * *"
  time_zone        = "Europe/Riga"
  attempt_deadline = "120s"

  retry_config {
    retry_count = 5
  }

  http_target {
    uri         = google_cloudfunctions_function.update.https_trigger_url
    http_method = "GET"

    oidc_token {
      service_account_email = google_service_account.scheduler_account.email
    }
  }
}
