#!/usr/bin/env python
import argparse
import covid.download
import covid.clean
import covid.preprocess
import covid.upload

def get(request, skip_upload=False):
    try:
        covid.download.main()
        covid.clean.main()
        covid.preprocess.main()
        if not skip_upload:
            covid.upload.main()
    except Exception as e:
        return repr(e)
    return "Success"

def main():  # pylint: disable=missing-function-docstring
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser.add_argument(
        "--skip-upload", 
        action="store_true",
        default=False,
        help="Skip uploading to cloud bucket.",
    )
    args = parser.parse_args()
    print(get(None, args.skip_upload))

if __name__ == "__main__":
    main()
