import os
from pathlib import Path

def create_parent_dir(filepath):
    parent_dir = os.path.split(filepath)[0]
    Path(parent_dir).mkdir(parents=True, exist_ok=True)