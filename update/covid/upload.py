#!/usr/bin/env python
from google.cloud import storage
import glob
import os
import yaml

def main():  # pylint: disable=missing-function-docstring
    configs = yaml.safe_load(open("configs.yaml", "r"))
    client = storage.Client()
    bucket = client.bucket(configs["gcp"]["bucket"])

    cwd = os.getcwd()
    # Hacky way of moving to the parent of data/ folder
    os.chdir(os.getenv("RAW_DATA_PATH"))
    os.chdir("../..")
    try:
        for filepath in glob.glob("data/**", recursive=True):
            if os.path.isfile(filepath):
                print(f"Uploading {filepath:s}.")
                blob = bucket.blob(filepath)
                blob.upload_from_filename(filepath, content_type="text/csv")
    except Exception as e:
        os.chdir(cwd)
        raise e
    else:
        os.chdir(cwd)


if __name__ == "__main__":
    main()
