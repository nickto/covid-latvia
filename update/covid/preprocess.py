import os
import re

import pandas as pd
import yaml

from .helpers import create_parent_dir


def get_n_day_sum(source_col, target_col, df, n=14):
    df = df.copy()
    df.loc[:, target_col] = df.loc[:, source_col].rolling(n).sum()
    # Find indices because we subset by .iloc
    target_idx = df.columns.get_loc(target_col)
    source_idx = df.columns.get_loc(source_col)
    # Rewrite first n observations with regular cumsum, because rolling sum
    # leaves NAs in the first n - 1 places
    df.iloc[:n, target_idx] = df.iloc[:n, source_idx].expanding().sum()
    return df


def get_n_day_mean(source_col, target_col, df, n=14):
    df = df.copy()
    df.loc[:, target_col] = df.loc[:, source_col].rolling(n).mean()
    # Find indices because we subset by .iloc
    target_idx = df.columns.get_loc(target_col)
    source_idx = df.columns.get_loc(source_col)
    # Rewrite first n observations with regular cummean, because rolling mean
    # leaves NAs in the first n - 1 places
    df.iloc[:n, target_idx] = df.iloc[:n, source_idx].expanding().mean()
    return df


def preprocess_population(df):
    return df.set_index("name")


def preprocess_cases(df):
    # Convert positivy rate to decimal from percentage
    df.loc[:, "positivity_rate"] = df.loc[:, "positivity_rate"] / 100
    # Remove trailing dot in dates, so it is parsable
    df.loc[:, "date"] = df.loc[:, "date"].apply(lambda d: d[:-1])
    # Convert string date to actual date
    df.loc[:, "date"] = pd.to_datetime(df.loc[:, "date"])
    # Fill in missing dates (if any)
    df = df.sort_values("date")
    df = df.set_index("date")
    df = df.asfreq("D")
    df = df.reset_index()
    df.loc[:, "date"] = df.loc[:, "date"].apply(lambda d: d.date())
    return df


def preprocess_vaccination(df):
    # yapf: disable
    df.loc[:, "date"] = (
        df
        .loc[:, "date"]
        .apply(lambda x: pd.to_datetime(x).date())
    )

    # Aggregate
    groupby_cols = [
        "institution_code",
        "date",
        "vaccine_name",
        "vaccination_round",
    ]
    df = (
        df
        .groupby(groupby_cols, as_index=False)
        .agg({"count": "sum"})
    )

    # Add missing combinations
    idx = pd.MultiIndex.from_product(
        [df.loc[:, col].unique() for col in groupby_cols],
        names=groupby_cols
    )
    df = (
        df
        .set_index(groupby_cols)
        .reindex(idx, fill_value=0)
        .reset_index()
    )

    # Fix "1.pote" into "1"
    regex = re.compile(r"\d*")
    df.loc[:, "vaccination_round"] = (
        df
        .loc[:, "vaccination_round"]
        .apply(lambda x: regex.search(x)[0])
        .astype(int)
    )
    # yapf: enable
    return df


def summarize_vaccination_by_round(df):
    df_by_round = (df.loc[:, ["date", "vaccination_round", "count"]].groupby([
        "date", "vaccination_round"
    ]).sum().reset_index().pivot(index="date",
                                 columns=["vaccination_round"],
                                 values=["count"]))
    df_by_round.columns = df_by_round.columns.to_flat_index()
    df_by_round = df_by_round.rename(
        {
            ("count", 1): "round_1",
            ("count", 2): "round_2"
        }, axis=1)
    df_by_round.loc[:, "total"] = df_by_round.sum(axis=1)
    df_by_round_cum = df_by_round.copy().cumsum()
    df_by_round = df_by_round.merge(df_by_round_cum,
                                    on="date",
                                    suffixes=("", "_cum"))
    return df_by_round


def summarize_vaccination_by_vaccine(df):
    df = df.copy()
    df = df.groupby(["vaccine_name", "vaccination_round"])\
        .agg(count=("count", "sum"))
    df = df.reset_index()

    df.loc[:, "vaccine_name"] = df.loc[:, "vaccine_name"].replace({
        "COVID-19 Vaccine AstraZeneca":
        "AstraZeneca",
        "COVID-19 Vaccine Moderna":
        "Moderna",
        "COVID-19 Vaccine Janssen":
        "Janssen",
    })
    return df


def preprocess_hospitalizations(df):
    # Fix dates
    # yapf: disable
    df.loc[:,"date"] = (
        df.loc[:, "date"]
        .apply(lambda x: pd.to_datetime(x).date())
    )
    # yapf: enable

    # Compute aggregates
    df = df.groupby("date").agg(
        hospitalized=("hospitalized", "sum"),
        hospitalized_severe=("hospitalized_severe", "sum"),
        hospitalized_moderate=("hospitalized_moderate", "sum"),
    )

    # Add missing dates, if needed
    idx = pd.date_range(df.index.min(), df.index.max())
    df = df.reindex(idx, method="backfill")
    df.index = df.index.rename("date")

    return df


def main():
    # yapf: disable
    # Read in configs
    configs = yaml.safe_load(open("configs.yaml", "r"))

    # # Preprocess population
    filename = configs["data"]["population"]["clean"]["filename"]
    filepath = os.path.join(os.getenv("CLEAN_DATA_PATH"), filename)
    population = pd.read_csv(filepath)
    population = preprocess_population(population)

    # Preprocess cases
    filename = configs["data"]["cases"]["clean"]["filename"]
    filepath = os.path.join(os.getenv("CLEAN_DATA_PATH"), filename)
    cases = pd.read_csv(filepath)
    cases = preprocess_cases(cases)

    # Preprocess vaccination
    filename = configs["data"]["vaccination"]["clean"]["filename"]
    filepath = os.path.join(os.getenv("CLEAN_DATA_PATH"), filename)
    vaccination = pd.read_csv(filepath)
    vaccination = preprocess_vaccination(vaccination)
    vaccination_categories = vaccination

    vaccination_by_round = summarize_vaccination_by_round(vaccination)
    vaccination_by_vaccine = summarize_vaccination_by_vaccine(vaccination)
    vaccination_daily = vaccination_by_round

    # Preprocess hospitalizations
    filename = configs["data"]["hospitalizations"]["clean"]["filename"]
    filepath = os.path.join(os.getenv("CLEAN_DATA_PATH"), filename)
    hospitalizations = pd.read_csv(filepath)
    hospitalizations = preprocess_hospitalizations(hospitalizations)

    # Compute 14-day cumulative indicators
    cases = get_n_day_sum("cases", "cases_14_days_sum", cases)
    cases = get_n_day_sum("deaths", "deaths_14_days_sum", cases)
    cases = get_n_day_sum("tests", "tests_14_days_sum", cases)

    # Compute 14-day mean indicators
    cases = get_n_day_mean("cases", "cases_14_days_mean", cases)
    cases = get_n_day_mean("deaths", "deaths_14_days_mean", cases)
    cases = get_n_day_mean("positivity_rate", "positivity_rate_14_days_mean",
                           cases)

    # Compute per 100K indicators
    population_latvia = population.loc["VISA LATVIJA", "total"]
    multiplier = (1e5 / population_latvia)

    col = "cases_14_days_sum"
    cases.loc[:, f"{col:s}_per_100K"] = cases.loc[:, col] * multiplier

    col = "deaths_14_days_sum"
    cases.loc[:, f"{col:s}_per_100K"] = cases.loc[:, col] * multiplier

    col = "tests_14_days_sum"
    cases.loc[:, f"{col:s}_per_100K"] = cases.loc[:, col] * multiplier

    cases_daily = cases

    # Save
    filename = configs["processed"]["cases_daily"]["filename"]
    filepath = os.path.join(os.getenv("PROCESSED_DATA_PATH"), filename)
    create_parent_dir(filepath)
    cases_daily.to_csv(filepath)
    print(f"Cases daily saved to {filepath}.")

    filename = configs["processed"]["vaccination_daily"]["filename"]
    filepath = os.path.join(os.getenv("PROCESSED_DATA_PATH"), filename)
    create_parent_dir(filepath)
    vaccination_daily.to_csv(filepath)
    print(f"Vaccinations daily saved to {filepath}.")

    filename = configs["processed"]["vaccination_categories"]["filename"]
    filepath = os.path.join(os.getenv("PROCESSED_DATA_PATH"), filename)
    create_parent_dir(filepath)
    vaccination_categories.to_csv(filepath)
    print(f"Vaccination categories saved to {filepath}.")

    filename = configs["processed"]["vaccination_by_vaccine"]["filename"]
    filepath = os.path.join(os.getenv("PROCESSED_DATA_PATH"), filename)
    create_parent_dir(filepath)
    vaccination_by_vaccine.to_csv(filepath)
    print(f"Vaccination by vaccine saved to {filepath}.")

    filename = configs["processed"]["hospitalizations"]["filename"]
    filepath = os.path.join(os.getenv("PROCESSED_DATA_PATH"), filename)
    create_parent_dir(filepath)
    hospitalizations.to_csv(filepath)
    print(f"Hospitalizations saved to {filepath}.")
    # yapf: enable


if __name__ == "__main__":
    main()
